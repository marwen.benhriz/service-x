﻿using Solution_X.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Solution_X.Core.Services
{
    public interface IPictureService
    {
        Task<IEnumerable<Picture>> GetAllPictures();

        Task<Picture> GetPictureById(int id);

        Task<Picture> CreatePicture(Picture newPicture);

        Task UpdatePicture(Picture pictureToBeUpdated, Picture picture);

        Task DeletePicture(Picture picture);
    }
}
