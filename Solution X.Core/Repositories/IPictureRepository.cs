﻿using Solution_X.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Solution_X.Core.Repositories
{
    public interface IPictureRepository : IRepository<Picture>
    {
        Task<IEnumerable<Picture>> GetAllWithPictureAsync();
        Task<Picture> GetWithPictureByIdAsync(int id);
    }
}
