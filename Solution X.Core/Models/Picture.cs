﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Solution_X.Core.Models
{
    public class Picture
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        public DateTime TakenDate { get; set; }

    }
}
