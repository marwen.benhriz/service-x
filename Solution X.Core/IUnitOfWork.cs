﻿using Solution_X.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Solution_X.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IPictureRepository Pictures { get; }

        IUserRepository Users { get; }

        Task<int> CommitAsync();
    }
}
