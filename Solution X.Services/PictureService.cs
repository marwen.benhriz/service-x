﻿using Solution_X.Core;
using Solution_X.Core.Models;
using Solution_X.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Solution_X.Services
{
    public class PictureService : IPictureService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PictureService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Picture> CreatePicture(Picture newPicture)
        {
            await _unitOfWork.Pictures
                .AddAsync(newPicture);
            await _unitOfWork.CommitAsync();

            return newPicture;
        }

        public async Task DeletePicture(Picture picture)
        {
            _unitOfWork.Pictures.Remove(picture);

            await _unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<Picture>> GetAllPictures()
        {
            return await _unitOfWork.Pictures.GetAllAsync();
        }

        public async Task<Picture> GetPictureById(int id)
        {
            return await _unitOfWork.Pictures.GetByIdAsync(id);
        }

        public async Task UpdatePicture(Picture pictureToBeUpdated, Picture picture)
        {
            pictureToBeUpdated.Name = picture.Name;

            await _unitOfWork.CommitAsync();
        }
    }
}
