## Requirements

### framework/packages
1) Dotnet Core 3.1.4

### Security
In compliance with High and Strict security implementations of [ASP.NET core](https://docs.microsoft.com/en-us/aspnet/core/security/?view=aspnetcore-3.1)

1) Enforce HTTPS
2) prevent open redirect attacks
3) Enable CORS
4) shared cookies (SSO)
5) SameSite Cookies (protection against CSRF)
6) IP safelist
7) App Security (OWASP)