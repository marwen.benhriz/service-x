#!/bin/bash
APP_NAME=${APP_NAME:-solution-x}

export ASPNETCORE_ENVIRONMENT=${ASPNETCORE_ENVIRONMENT:-Development}
export SERVER_PORT_ENV=${SERVER_PORT_ENV:-7576}

start () {  
  VERBOSITY=${VERBOSITY:-normal}
  dotnet watch run -v $VERBOSITY
}

# ./do.sh build_push [project_id]
build_push () {
    PROJECT_ID=${PROJECT_ID:-$1}
    TAG=${TAG:-latest}

    GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o bin/linux_amd64 -v
    docker build -t gcr.io/${PROJECT_ID}/beee-${APP_NAME}:${TAG} --no-cache --file=`pwd`/docker/app/Dockerfile .
    gcloud docker -- push gcr.io/${PROJECT_ID}/beee-${APP_NAME}:${TAG}
}

generate_k8s_config () {
    mkdir -p ./docker/k8s/configs
    APPSETTINGS_FILE=${APPSETTINGS_FILE:-appsettings.json}
    export APPSETTINGS_CONT=$(sed 's/^/    /' ./${APPSETTINGS_FILE})

    cat > ./docker/k8s/configs/solution-x-configs.yml <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: solution-x-configs
  labels:
    app: solution-x
    component: microservice
    role: solution-x
data:
  $APPSETTINGS_FILE: |-
$APPSETTINGS_CONT
EOF
}

$*