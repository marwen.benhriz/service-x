﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Solution_X.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Solution_X.Data.Configuration
{
    public class PictureConfiguration : IEntityTypeConfiguration<Picture>
    {
        public void Configure(EntityTypeBuilder<Picture> builder)
        {
            builder
                .HasKey(p => p.Id);

            builder
                .Property(p => p.Id)
                .UseIdentityColumn();

            builder
                .Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(p => p.Location)
                .IsRequired()
                .HasMaxLength(255);

            builder
                .Property(p => p.TakenDate)
                .IsRequired()
                .HasMaxLength(255);

            builder
                .ToTable("Pictures");
        }
    }
}
