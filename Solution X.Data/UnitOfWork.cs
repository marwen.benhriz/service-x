﻿using Solution_X.Core;
using Solution_X.Core.Repositories;
using Solution_X.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Solution_X.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Solution_X_DbContext _context;
        private IPictureRepository _pictureRepository;
        private IUserRepository _userRepository;

        public UnitOfWork(Solution_X_DbContext context)
        {
            this._context = context;
        }
        public IPictureRepository Pictures => _pictureRepository = _pictureRepository ?? new PictureRepository(_context);

        public IUserRepository Users => _userRepository = _userRepository ?? new UserRepository(_context);

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
