﻿using Microsoft.EntityFrameworkCore;
using Solution_X.Core.Models;
using Solution_X.Data.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Solution_X.Data
{
    public class Solution_X_DbContext : DbContext
    {
        public DbSet<Picture> Pictures { get; set; }

        public DbSet<User> Users { get; set; }

        public Solution_X_DbContext(DbContextOptions<Solution_X_DbContext> options)
       : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .ApplyConfiguration(new PictureConfiguration());

            builder
                .ApplyConfiguration(new UserConfiguration());

        }
    }
}
