﻿using Microsoft.EntityFrameworkCore;
using Solution_X.Core.Models;
using Solution_X.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Solution_X.Data.Repositories
{
    public class PictureRepository : Repository<Picture>, IPictureRepository
    {
        private Solution_X_DbContext Solution_X_DbContext
        {
            get { return Context as Solution_X_DbContext; }
        }
        public PictureRepository(Solution_X_DbContext context)
       : base(context)
        { }
        
        public async Task<IEnumerable<Picture>> GetAllWithPictureAsync()
        {
            return await Solution_X_DbContext.Pictures
                .ToListAsync();
        }

        public Task<Picture> GetWithPictureByIdAsync(int id)
        {
            return Solution_X_DbContext.Pictures
                .SingleOrDefaultAsync(p => p.Id == id);
        }

        async Task<IEnumerable<Picture>> IPictureRepository.GetAllWithPictureAsync()
        {
            return await Solution_X_DbContext.Pictures
              .ToListAsync();
        }

        Task<Picture> IPictureRepository.GetWithPictureByIdAsync(int id)
        {
            return Solution_X_DbContext.Pictures
         .SingleOrDefaultAsync(p => p.Id == id);
        }

    }
}
