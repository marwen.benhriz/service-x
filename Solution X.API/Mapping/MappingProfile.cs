﻿using AutoMapper;
using Solution_X.API.Resources;
using Solution_X.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solution_X.API.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain(base de donnée )  vers Resource
            CreateMap<User, UserResource>();


            // Resources vers Domain ou la base de données
            CreateMap<UserResource, User>();





        }

    }
}
