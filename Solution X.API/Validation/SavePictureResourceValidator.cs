﻿using FluentValidation;
using Solution_X.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solution_X.API.Validation
{
    public class SavePictureResourceValidator : AbstractValidator<Picture>
    {
        public SavePictureResourceValidator()
        {
            RuleFor(p => p.Name)
             .NotEmpty()
             .MaximumLength(50);
        }
    }
}
