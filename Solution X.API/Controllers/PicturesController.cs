﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Solution_X.API.Validation;
using Solution_X.Core.Models;
using Solution_X.Core.Services;

namespace Solution_X.API.Controllers
{
    [Route("api/pictures")]
    [ApiController]
    public class PicturesController : ControllerBase
    {
        private readonly IPictureService _servicePicture;

        public PicturesController(IPictureService servicePicture)
        {
            _servicePicture = servicePicture;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<Picture>>> GetAllPictures()
        {
            var pictures = await _servicePicture.GetAllPictures();
            return Ok(pictures);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Picture>> GetPictureById(int id)
        {
            try
            {
                var picture = await _servicePicture.GetPictureById(id);
                if (picture == null) return BadRequest("This image not found");
                return Ok(picture);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("")]
        public async Task<ActionResult<Picture>> CreatePicture(Picture newPicture)
        {
            // validation
            var validation = new SavePictureResourceValidator();
            var validationResult = await validation.ValidateAsync(newPicture);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors);
            // Creation picture
            var pictureNew = await _servicePicture.CreatePicture(newPicture);
            // mappage
            return Ok(pictureNew);
        }

        [HttpPut("")]
        public async Task<ActionResult<Picture>> UpdatePicture(int id, Picture picture)
        {
            // validation
            var validation = new SavePictureResourceValidator();
            var validationResult = await validation.ValidateAsync(picture);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors);
            // Get arist by ID
            var pictureUpdate = await _servicePicture.GetPictureById(id);
            if (pictureUpdate == null) return NotFound();
            // update Artist
            await _servicePicture.UpdatePicture(pictureUpdate, picture);
            //get artistBy id
            var pictureNew = await _servicePicture.GetPictureById(id);

            return Ok(pictureNew);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePicture(int id)
        {
            var picture = await _servicePicture.GetPictureById(id);
            if (picture == null) return NotFound();
            await _servicePicture.DeletePicture(picture);
            return NoContent();
        }
    }
}
